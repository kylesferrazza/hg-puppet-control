# Class: hourglass
#
#
class hourglass {
  include 'podman'
  podman::pod { 'hourglass-pod':
    flags => {
      publish => [
        '80:80',
        '6379:6379',
      ],
    },
  }
  podman::container { 'hourglass':
    image => 'ghcr.io/codegrade/hourglass:docker-ci-a7294ef2d6d9d4edcd896d8af91b48925823c96c',
    flags => {
      name => 'hourglass',
      pod  => 'hourglass-pod',
      env  => [
        'HOURGLASS_DATABASE_HOST=postgres',
        'SECRET_KEY_BASE=aaaaaa',
        'BOTTLENOSE_URL=http://bottlenose',

        # these are local testing creds
        'BOTTLENOSE_APP_ID=YYdiyTMC4HRH9WpTvrFmpRHAf8xY09c67woaNzbI1OQ',
        'BOTTLENOSE_APP_SECRET=RIu1dprvaQ5swuvtYXOvqNvbt3CbANaAr5KA1Eg-cpk',
      ],
    },
  }
  podman::container { 'redis':
    image => 'docker.io/redis:6.2.6',
    flags => {
      name => 'redis',
      pod  => 'hourglass-pod',
    },
  }
}
